#include "Vector.h"
#include <iostream>
#include<string>

Vector::Vector(int n)
{
	
	if (n < 2)
		n = 2;
	_elements = new int[n];
	_size = 0;
	_capacity = n;
	_resizeFactor = n;

}
Vector::~Vector()
{
	delete _elements;

}

int Vector::size() const
{
	return _size;
}


int Vector::capacity() const
{
	return _capacity;
}

int Vector::resizeFactor() const
{
	return _resizeFactor;
}

bool Vector::empty()
{
	return !_size;
}

// / / / / / / / / / / / 
void Vector::push_back(const int& val)
{
	//TO DO
	if (_size == _capacity)
	{
		//TO DO
		//Capa + resizeFactor
		int* temps = new int[_size];//
		
		for (size_t i = 0; i < _size; ++i)
			temps[i] = _elements[i];//
		delete _elements;
		_capacity += _resizeFactor;
		_elements = new int[_capacity];
		for (size_t i = 0; i < _size; ++i)
			_elements[i] = temps[i];
		delete temps;
		
	}
	
	_elements[_size++] = val;
	

}

int Vector::pop_back()
{
	if (empty())
	{
		std::cerr << "Error: pop from empty vetor" << std::endl;
		return -9999;
	}
	_size--;
	int val = _elements[_size];
	
	_elements[_size] = NULL;
	
	
	return val;
}

void Vector::reserve(int n)
{
	while (_capacity < n)
		_capacity += _resizeFactor;
	int* temps = new int(_size);
	for (size_t i = 0; i < _size; ++i)
		temps[i] = _elements[i];
	delete _elements;
	_elements = new int(_capacity);
	for (size_t i = 0; i < _size; ++i)
		_elements[i] = temps[i];
	delete temps;
}

void Vector::resize(int n, const int& val)
{
	resize(n);
	assign(val);
}

int& Vector::operator[](int n) const
{
	if (n >= _size || n < 0)
	{
		std::cerr << "Error: index out of range" << std::endl;
		return _elements[0];
	}
	return _elements[n];
}

void Vector::resize(int n)
{
	if (n <= _capacity)
	{
		int* temps = new int(n);
		for (size_t i = 0; i < _size; ++i)
			temps[i] = _elements[i];
		delete _elements;
		_elements = new int(n);
		for (size_t i = 0; i < _size; ++i)
			_elements[i] = temps[i];
		delete temps;
		_capacity = n;
		if (_size > _capacity)
		{
			_size = _capacity;
		}
	}
	else
		reserve(n);
	
	
}
void Vector::assign(int val)
{
	for (size_t i = 0; i < _size; i++)
	{
		this->_elements[i] = val;
	}
}

Vector::Vector(const Vector& other)
{
	
	*this = other;
	/*_elements = new int(other._capacity);
	for (size_t i = 0; i < other._size; ++i)
		_elements[i] = other._elements[i];
	_capacity = other._capacity;
	_size = other._size;*/
	
	
	
	
}
Vector& Vector::operator=(const Vector& other)
{
	
	_elements = new int[other._capacity];
	for (size_t i = 0; i < other._size; ++i)
		_elements[i] = other._elements[i];
	_capacity = other._capacity;
	_size = other._size;
	_resizeFactor = other._resizeFactor;
	return *this;
}

Vector& Vector::operator+=( const Vector& other)
{
	
	int size_t = 0;
	
	if (other._capacity > this->_capacity)
		this->resize(other._capacity);
	other._size < this->_size ? size_t = this->_size : size_t = other._size;
	this->_size = size_t;
	for (int i = 0; i < size_t; i++)
	{
		this->_elements[i] = this->_elements[i] + other._elements[i];
	}

	return *this;

	// TODO: insert return statement here
}

Vector& Vector::operator-=(const Vector& other)
{
	int size_t = 0;

	if (other._capacity > this->_capacity)
		this->resize(other._capacity);
	other._size < this->_size ? size_t = this->_size : size_t = other._size;
	this->_size = size_t;
	for (int i = 0; i < size_t; i++)
	{
		this->_elements[i] = this->_elements[i] - other._elements[i];
	}
	return *this;
	// TODO: insert return statement here
}



Vector Vector::operator+( Vector const& other)const
{
	Vector copy(*this);
	copy += other;
	return copy;
	// TODO: insert return statement here
}

Vector Vector::operator-( Vector& other)
{
	Vector copy(*this);
	copy -= other;
	return copy;
	// TODO: insert return statement here
}

std::ostream& operator<<(std::ostream& os, Vector const& value)
{
	std::string msg = "Vector Info:\nCapacity is ";
	msg += std::to_string(value.capacity());
	msg += "\nSize is ";
	msg += std::to_string(value.size());
	msg += " data is ";
	for (size_t i = 0; i < value.size(); i++)
	{
		msg += std::to_string(value[i]);
		msg += ", ";
	}
	
	return os << msg << std::endl;



	// TODO: insert return statement here
}
