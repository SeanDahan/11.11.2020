#pragma once
#include <iostream>
class Vector
{
public:
	Vector(int n);
	Vector(const Vector& other);
	~Vector();
	int size() const;
	int capacity() const;
	int resizeFactor() const;
	bool empty();
	// / / / / / / / / / / / / / /
	void push_back(const int& val);
	int pop_back();
	void assign(int val);
	void resize(int n);
	void reserve(int n);
	void resize(int n, const int& val);
	int& operator[](int n) const;
	Vector& operator=(const Vector& other);
	
	Vector operator-( Vector& other);
	Vector operator+(Vector const& other) const;
	Vector& operator+=( const Vector& other);
	Vector& operator-=( const Vector& other);

	friend std::ostream& operator<<(std::ostream& os, Vector const& value);
private:
	int* _elements;
	int _size;
	int _capacity;
	int _resizeFactor;
};

